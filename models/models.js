const Sequelize = require('sequelize');
const connection = new Sequelize('c9','root','p@ss',{
    dialect: 'mysql',
    logging: false,
    define: {
        timestamps: false
    }
});

connection.authenticate()
    .then(() => console.log('Successfully connected to database...'))
    .catch(err => console.log(`Could not connect to database, error: ${err}.`));

class Food extends Sequelize.Model{};
class Category extends Sequelize.Model{};
class Group extends Sequelize.Model{};
class User extends Sequelize.Model{};
class UserFood extends Sequelize.Model{};

Category.init({
    id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
    name: { type: Sequelize.STRING },
}, { sequelize: connection, modelName: 'Categories' });

Group.init({
    id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
    name: { type: Sequelize.STRING }
}, { sequelize: connection, modelName: 'Groups' })

Food.init({
    id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
    name: { type: Sequelize.STRING }
}, { sequelize: connection, modelName: 'Food'});

User.init({
    id: { type: Sequelize.INTEGER, primaryKey:true, autoIncrement: true },
    firstName: { type: Sequelize.STRING },
    lastName: { type: Sequelize.STRING },
    phoneNumber: { type: Sequelize.STRING },
    email: { type: Sequelize.STRING },
    password: { type: Sequelize.STRING }
}, { sequelize: connection, modelName: 'Users' });

UserFood.init({
    id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
    addedDate: { type: Sequelize.DATEONLY },
    expirationDate: { type: Sequelize.DATEONLY },
    quantity: { type: Sequelize.STRING },
    isAvailable: { type: Sequelize.INTEGER },
}, { sequelize: connection, modelName: 'UserFood' });

// Relations Definitions

Category.hasMany(Food, { as: 'food', foreignKey: { name: 'categoryId', allowNull: true }, sourceKey: 'id' });

Group.belongsToMany(User, { through: 'UserGroup', as: 'users', foreignKey: 'groupId' });

Food.belongsToMany(User, { through: UserFood, as: 'foodOwners', foreignKey: 'foodId' });
Food.belongsTo(Category, { as: 'category', foreignKey: { name: 'categoryId', allowNull: true }, targetKey: 'id' });

User.belongsToMany(Food, { through: UserFood, as: 'userFood', foreignKey: 'userId' });
User.belongsToMany(Group, { through: 'UserGroup', as: 'groups', foreignKey: 'userId' });

async function createMockData(addData) {
    if(addData)
    {
        await Category.bulkCreate([
            { name: 'Fruits' },
            { name: 'Pasta' },
            { name: 'Meat' },
            { name: 'Vegetables' },
            { name: 'Cereals' },
            { name: 'Dairy' }], { returning : true }).then((result)  => { 
                //console.log(result);
        });

        await Food.bulkCreate([
            { name: 'Strawberry', categoryId: 1 },
            { name: 'Peach', categoryId: 1 },
            { name: 'Apple', categoryId: 1 },
            { name: 'Lasagna', categoryId: 2 },
            { name: 'Carbonara', categoryId: 2 },
            { name: 'Bolognese', categoryId: 2 },
            { name: 'Pork Steak', categoryId: 3 },
            { name: 'Chicken Breast', categoryId: 3 },
            { name: 'Fried Fish', categoryId: 3 },
            { name: 'Tomato', categoryId: 4 },
            { name: 'Carrot', categoryId: 4 },
            { name: 'Cucumber', categoryId: 4 },
            { name: 'Oats', categoryId: 5 },
            { name: 'Bread', categoryId: 5 },
            { name: 'Corn', categoryId: 5 },
            { name: 'Milk', categoryId: 6},
            { name: 'Cheese', categoryId: 6},
            { name: 'Yogurt', categoryId: 6}
        ], { returning: true}).then((result) => {
            //console.log(result);
        });
        
        await Group.bulkCreate([
            { name: 'Meat Masters' }, // id: 1
            { name: 'The Gouda Life' },
            { name: 'The Cereal Killers' },
            { name: 'Pasta Lovers' },
            { name: 'Veggie Cuisine' },
            { name: 'Apple Pie' }
        ], { returning: true }).then((result) => {
            //console.log(result);
        });
        
        await User.bulkCreate([
            { firstName: 'John', lastName: 'Doe', phoneNumber: '0723456712', email: 'john.doe@gmail.com', password: 'abc123' }, 
            { firstName: 'Michael', lastName: 'Morris', phoneNumber: '0728976712', email: 'michael.morris@gmail.com', password: 'bbb234' },
            { firstName: 'Anne', lastName: 'Marie', phoneNumber: '0723456333', email: 'anne.marie@gmail.com', password: 'c6d5' },
            { firstName: 'Andrew', lastName: 'Doe', phoneNumber: '0723569712', email: 'andrew.doe@gmail.com', password: 'j474g'}, 
            { firstName: 'Michelle', lastName: 'Phan', phoneNumber: '0723129712', email: 'michelle.phan@gmail.com', password: 'b9kg3'}, 
        ], { returning: true }).then((result) => 
        {
            for(let i = 0; i < result.length; i++){
                if(i == 0)
                    result[i].setGroups([ 1, 3 ]);
                else if(i == 1)
                    result[i].setGroups([ 2, 5 ]);
                else if(i == 2)
                    result[i].setGroups([ 2, 6 ]);
                else if(i == 4)
                    result[i].setGroups([ 4, 3 ]);
            }
        });
        
        await UserFood.bulkCreate([
            { addedDate: new Date(Date.UTC(2018, 0, 2)), expirationDate: new Date(Date.UTC(2018, 3, 4)), quantity: '1 kg ', isAvailable: 1, foodId: 1, userId: 2 },
            { addedDate: new Date(Date.UTC(2018, 2, 2)), expirationDate: new Date(Date.UTC(2018, 4, 2)), quantity: '200 g ', isAvailable: 1, foodId: 15, userId: 1 },
            { addedDate: new Date(Date.UTC(2018, 5, 7)), expirationDate: new Date(Date.UTC(2019, 8, 2)), quantity: '500 g ', isAvailable: 0, foodId: 4, userId: 4 },
            { addedDate: new Date(Date.UTC(2019, 0, 1)), expirationDate: new Date(Date.UTC(2020, 0, 2)), quantity: '200 g ', isAvailable: 1, foodId: 6, userId: 5 },
            { addedDate: new Date(Date.UTC(2019, 3, 2)), expirationDate: new Date(Date.UTC(2019, 3, 29)), quantity: '450 g ', isAvailable: 1, foodId: 12, userId: 3 },
            { addedDate: new Date(Date.UTC(2019, 4, 13)), expirationDate: new Date(Date.UTC(2019, 11, 2)), quantity: '300 g ', isAvailable: 1, foodId: 13, userId: 2 },
            { addedDate: new Date(Date.UTC(2018, 11, 2)), expirationDate: new Date(Date.UTC(2019, 11, 28)), quantity: '600g ', isAvailable: 1, foodId: 9, userId: 3 }
            ], {returning: true }).then((result)=>{
                //console.log(result);
        });
        
        console.log('Inserted mock data succesfully.');
    }
}

connection.query('SET FOREIGN_KEY_CHECKS = 0', null, {raw: true})
    .then(function(results) {
        connection.sync({ force: true })
        .then(async () => 
        {
            await createMockData(true);
            console.log('Database created succesfully.');
        })
        .catch(err => console.log('Failed to create database, error: ' + err));
    })
    .catch(err => console.log(`Could not recreate database, error: ${err}`));

module.exports = {
    connection,
    Food,
    Category,
    Group,
    User,
    UserFood
}