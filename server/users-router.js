const usersRouter = require('express').Router();
const Op = require('sequelize').Op;
const {User, UserFood, Group, Category, Food } = require('../models/models');

usersRouter.get('/', (req, res) => {
    return User.findAll(/*{ attributes: ['firstName', 'lastName', 'phoneNumber', 'email']}*/).then((users) => res.status(200).send(users))
    .catch(err => res.status(500).send({
        message: err
    }));
});


usersRouter.get('/:id', (req, res) => {
    let id = parseInt(req.params.id);
    if(isNaN(id))
        return res.status(400).send({message: 'Incorrect request.'});
    else
        return User.findAll({ 
            where: { id: id },
            include: [ 
                { model: Food, as: 'userFood', through: { model: UserFood, as: 'details', attributes: [ 'addedDate', 'expirationDate', 'quantity', 'isAvailable' ] } },
                { model: Group, as: 'groups', through: { attributes: [] } }
            ]
        }).then(users => 
        {
            return res.status(200).send(users);
        })
        .catch(err => 
        {
            console.log(err); 
            return res.status(500).send({ message: 'Could not find user.' });
        });
});


usersRouter.post('/', (req, res) => {
    let user = req.body;
    if(typeof user === 'undefined' || user == null 
        || user.firstName == undefined || user.firstName == null || user.firstName.trim() === ''
        || user.lastName == undefined || user.lastName == null || user.lastName.trim() === ''
        || user.phoneNumber == undefined || user.phoneNumber == null || user.phoneNumber.trim() === ''
        || user.email == undefined || user.email == null || user.email.trim() === ''
        || user.password == undefined || user.password == null || user.password.trim() === '')
        return res.status(400).send({ message: 'Invalid request.' });
    else 
        return User.create(user).then(() => res.status(201).send({ message: "User created." }))
        .catch(err => 
        {
            console.log(err);
            return res.status(500).send({ message: "Could not create user." });
        });
});


usersRouter.put('/:id', (req, res) => {
    let id = parseInt(req.params.id),
        user = req.body;
    if(isNaN(id))
        return res.status(400).send({message: 'Incorrect request.'});
    
    if((user.firstName == undefined || user.firstName == null || user.firstName.trim() === '')
        && (user.lastName == undefined || user.lastName == null || user.lastName.trim() === '')
        && (user.phoneNumber == undefined || user.phoneNumber == null || user.phoneNumber.trim() === '')
        && (user.email == undefined || user.email == null || user.email.trim() === '')
        && (user.password == undefined || user.password == null || user.password.trim() === ''))
        return res.status(400).send({message: 'Incorrect request'});
        
    return User.update(user, {where:{ id: id}}).then(() => res.status(200).
        send({message: 'User updated.'}))
        .catch(err =>{ console.log(err);
        return res.status(500).send({message: 'Invalid request.'});
        });
});

usersRouter.post('/:id/food', (req,res) => {
   let id = parseInt(req.params.id),
        food = req.body,
        userFood = [];
   if(isNaN(id))
        return res.status(400).send({message: 'Incorrect request.'});
    
    if(Array.isArray(food)){
        let incorrectEntries = food.filter(f => typeof f !== 'object' || f == null 
            || typeof f.id === 'undefined' || f.id == null || typeof f.id !== 'number'
            || typeof f.expirationDate === 'undefined' || f.expirationDate == null || typeof f.expirationDate !== 'string'
            || typeof f.quantity === 'undefined' || f.quantity == null || typeof f.quantity !== 'string');
        if(incorrectEntries.length > 0){
            return res.status(400).send({message: 'Incorrect request.'});
        }
        incorrectEntries = food.filter(f => {
            let date = Date.parse(f.expirationDate);
            if (isNaN(date))
                return true;
            return false;
        });
        if(incorrectEntries.length > 0)
            return res.status(400).send({message: 'Incorrect request.'});
        
        food.forEach(f => userFood.push({ addedDate: new Date(), expirationDate: new Date(Date.parse(f.expirationDate)), quantity: f.quantity, isAvailable: 1, foodId: f.id, userId: id }));
    
        return UserFood.bulkCreate(userFood).then(() => {
            return res.status(201).send({message: 'Added user food.'});
        }).catch(err => {
            console.log(err);
            return res.status(500).send({message: 'Could not add food to user.'});
        })
    }
    else
        return res.status(400).send({message: 'Incorrect request.'});
        
});

usersRouter.put('/:id/food', (req,res) => {
   let id = parseInt(req.params.id),
        food = req.body;
   if(isNaN(id))
        return res.status(400).send({message: 'Incorrect request.'});
    
    if(Array.isArray(food)){
        let incorrectEntries = food.filter(f => typeof f !== 'object' || f == null 
            || typeof f.userId === 'undefined' || f.userId == null || typeof f.userId !== 'number'
            || typeof f.foodId === 'undefined' || f.foodId == null || typeof f.foodId !== 'number');
        if(incorrectEntries.length > 0){
            return res.status(400).send({message: 'Incorrect request.'});
        }
        return UserFood.findAll({ raw: true, where: {userId: {[Op.in]: food.map(f => f.userId)}, foodId: {[Op.in]: food.map(f => f.foodId)}, isAvailable: 1 }})
        .then((userFood) => {
            let userFoodIds = userFood.map(uf => uf.id);
            for(let i = 0; i < userFood.length; i++){
                userFood[i].userId = id;
            } 

            return UserFood.destroy({ where: { id: { [Op.in]: userFoodIds } } }).then(() =>
            {
                return UserFood.bulkCreate(userFood).then(() => res.status(201).send({message: 'Added user food.'}))
                    .catch(err => {
                        console.log(err);
                        return res.status(500).send({message: 'Could not claim food for user.'});
                    });
            }).catch(err => {
                console.log(err);
                return res.status(500).send({message: 'Could not claim food for user.'});
            });
            
        }).catch(err => {
            console.log(err);
            return res.status(500).send({message: 'Could not find user\'s food.'});
        })
    }
    else
        return res.status(400).send({message: 'Incorrect request.'});
        
});

usersRouter.post('/:id/groups', (req,res) => {
   let id = parseInt(req.params.id),
        groups = req.body,
        userGroups = [];
   if(isNaN(id))
        return res.status(400).send({message: 'Incorrect request.'});
    
    if(Array.isArray(groups)){
        let incorrectEntries = groups.filter(g => typeof g !== 'object' || g == null 
            || typeof g.id === 'undefined' || g.id == null || typeof g.id !== 'number');
        if(incorrectEntries.length > 0){
            return res.status(400).send({message: 'Incorrect request.'});
        }
        
        userGroups = groups.map(g => g.id);
        return User.findAll({ where: { id: id } }).then((user) => user[0].setGroups(userGroups).then(() => {
                return res.status(201).send({message: 'Added user to groups.'});
            }).catch(err => {
                console.log(err);
                return res.status(500).send({message: 'Could not add user to groups.'});
        })).catch(err => {
            console.log(err);
            return res.status(500).send({message: 'Could not add user to groups.'});
        });
    }
    return res.status(400).send({message: 'Incorrect request.'});
});

usersRouter.delete('/:id', (req, res) => {
    let id = parseInt(req.params.id);
    if(isNaN(id))
        return res.status(400).send({message: 'Incorrect request.'});
    
    return User.destroy({ where: { id: id } }).then(() => res.status(200).send({ message: "User has been deleted. " }))
    .catch(err => 
    {
        console.log(err);
        return res.status(500).send({ message: "User could not be deleted. " });
    });
});


usersRouter.post('/login', (req, res) => {
     let user = req.body;
     
     if(typeof user === 'undefined' || user == null 
        || user.email == undefined || user.email == null || user.email.trim() === ''
        || user.password == undefined || user.password == null || user.password.trim() === '')
        return res.status(400).send({ message: 'Invalid request.' });
        
    return User.findAll({ where: { email: user.email, password: user.password }})
        .then(users => {
            if(users.length < 1){
                return res.status(400).send({ message: 'Invalid request.' });
            }
            return res.status(200).send({message: 'Authenticated.'});
        }).catch(err => {
            console.log(err);
            return res.status(500).send({message: 'Could not authenticate user.'})
        });
});

usersRouter.get('*', (req, res) => res.status(404).send({ message: 'Not Found' }));

module.exports = usersRouter;