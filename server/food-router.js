const foodRouter = require('express').Router();
const {User, UserFood, Group, Category, Food } = require('../models/models');

foodRouter.get('/', (req, res) => {
    if(typeof req.query.cat !== 'undefined' && typeof req.query.usr !== 'undefined')
        return Food.findAll({ include: [ 
                { model: User, as: 'foodOwners', through: { model: UserFood, as: 'details', attributes: [ 'addedDate', 'expirationDate', 'quantity', 'isAvailable' ] } },
                { model: Category, as: 'category' }
        ]}).then(food => 
        {
            return res.status(200).send(food);
        })
        .catch(err => 
        {
            console.log(err); 
            return res.status(500).send({ message: err });
        });
    else
        return Food.findAll().then((users) => res.status(200).send(users))
            .catch(err => res.status(500).send({
                message: err
            }));
});


foodRouter.get('/:id', (req, res) => {
     let id = parseInt(req.params.id);
    if(isNaN(id))
        return res.status(400).send({message: 'Incorrect request.'});
    else
        return Food.findAll({ 
            where: { id: id },
            include: [ 
                { model: User, as: 'foodOwners', through: { model: UserFood, as: 'details', attributes: [ 'addedDate', 'expirationDate', 'quantity', 'isAvailable' ] } },
                { model: Category, as: 'category' }
            ]
        }).then(food => 
        {
            return res.status(200).send(food);
        })
        .catch(err => 
        {
            console.log(err); 
            return res.status(500).send({ message: err });
        });
});


foodRouter.post('/', (req, res) => {
    let food = req.body;
    if(typeof food == undefined || food == null ||
       food.name == null || food.name.trim() == '' || food.categoryId == null || typeof food.categoryId !== 'number'){
           return res.status(400).send({message: 'Invalid request.'});
    }
    else
        return Food.create(food).then(() => res.status(201).send({message: 'Food created.'})).catch(err => {
            console.log(err);
            return res.status(500).send({message: 'Could not create food.'});
        });
});

foodRouter.put('/:id', (req, res) => {
    let id = parseInt(req.params.id),
        food = req.body;
    if(isNaN(id))
        return res.status(400).send({message: 'Incorrect request.'});
    
    if((food.id == null || typeof food.id == 'number') &&
        (food.name == null || food.name.trim() == '') && (food.categoryId == null || typeof food.categoryId == 'number')){
            return res.status(400).send({message: 'Invalid request.'});
        }
    
    return Food.update(food, {where:{ id: id}}).then(() => res.status(200).
        send({message: 'Food updated.'}))
        .catch(err => { console.log(err);
        return res.status(500).send({message: 'Invalid request.'});
        });
});


foodRouter.post('/:id/category', (req, res) => {
    let id = req.params.id;
    let category = req.body;
    if(isNaN(id))
        return res.status(400).send({message: 'Incorrect request.'});
    if(typeof category == undefined || category == null || category.id == null || typeof category.id !== 'number'){
        return res.status(400).send({message: 'Incorrect request.'});
    }
    else {
        
        return Food.findAll({where: {id: id}}).then((food) => {
            food.categoryId = category.id;
            Food.update(food, { where: { id: id } }).then(() => res.status(201).send({message: 'Food added to category.'}))
            .catch(err => {
                console.log(err);
                return res.status(500).send({message: 'Could not add food to category.'})
            });
        }).catch(err => {
            console.log(err);
            return res.status(500).send({message: 'Could not find food.'});
        })
    }
});

foodRouter.delete('/:id', (req, res) => {
    let id = parseInt(req.params.id);
    if(isNaN(id))
        return res.status(400).send({message: 'Incorrect request.'});
    
    return Food.destroy({ where: { id: id } }).then(() => res.status(200).send({ message: "Food has been deleted. " }))
    .catch(err => 
    {
        console.log(err);
        return res.status(500).send({ message: "Food could not be deleted. " });
    });
});

foodRouter.get('*', (req, res) => res.status(404).send({ message: 'Not Found' }));

module.exports = foodRouter;