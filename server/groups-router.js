const groupsRouter = require('express').Router();
const {User, UserFood, Group, Category, Food } = require('../models/models');

groupsRouter.get('/', (req, res) => {
    return Group.findAll().then((result) => res.status(200).send(result)).catch(err =>{
        console.log(err);
        return res.status(500).send({message: 'Could not get Groups.'});
    });
});

groupsRouter.get('/:id', (req, res) => {
    let id = req.params.id;
    if(isNaN(id)){
        return res.status(400).send({message: 'Incorrect request.'});
    }
    return Group.findAll({where: {id: id}, include: [ { model: User, as: 'users', through: { attributes: [] } } ]}).then((result) => res.status(200).send(result)).catch((err) => {
        console.log(err);
        return res.status(500).send({message: 'Could not find group'});
    }) 
});

groupsRouter.post('/', (req, res) => {
    let group = req.body;
    if(typeof group == undefined || group == null || group.name == null ||group.name.trim() === ''){
        return res.status(400).send({message: 'Incorrect request.'});
    }
    else return Group.create(group).then(() => {
        return res.status(201).send({message: 'Group created.'});
    }).catch(err => {
        console.log(err);
        return res.status(500).send({message: 'Could not create group.'});
    })
});

groupsRouter.put('/:id', (req, res) =>{
    let id = req.params.id;
    let group = req.body;
    if(isNaN(id))
        return res.status(400).send({message: 'Incorrect request.'});
    if(group.name == null || group.name.trim() === '')
        return res.status(400).send({message: 'Incorrect request.'});
    return Group.update(group, {where: {id: id}}).then(() => {
        return res.status(200).send({message: 'Group updated.'});
    }).catch(err => {
        console.log(err);
        return res.status(500).send({message: 'Invalid request.'});
    })
});

groupsRouter.delete('/:id', (req, res) => {
    let id = req.params.id;
    if(isNaN(id))
        return res.status(400).send({message: 'Incorrect request.'});
    return Group.destroy({where: {id: id}}).then(() => res.status(200).send({message: 'Group deleted.'})).catch(err => {
        console.log(err);
        return res.status(500).send({message: 'Invalid request'});
    })
});

groupsRouter.get('*', (req, res) => res.status(404).send({ message: 'Not Found' }));

module.exports = groupsRouter;