const categoriesRouter = require('express').Router();
const {User, UserFood, Group, Category, Food } = require('../models/models');

categoriesRouter.get('/', (req, res) => {
    return Category.findAll().then((result) => res.status(200).send(result)).catch(err =>{
        console.log(err);
        return res.status(500).send({message: 'Could not get Categories.'});
    });
});

categoriesRouter.get('/:id', (req, res) => {
    let id = req.params.id;
    if(isNaN(id)){
        return res.status(400).send({message: 'Incorrect request.'});
    }
    return Category.findAll({where: {id: id}, include: [ { model: Food, as: 'food' } ]}).then((result) => res.status(200).send(result)).catch((err) => {
        console.log(err);
        return res.status(500).send({message: 'Could not find category.'});
    }) 
});

categoriesRouter.post('/', (req, res) => {
    let category = req.body;
    if(typeof category == undefined || category == null || category.name == null ||category.name.trim() === ''){
        return res.status(400).send({message: 'Incorrect request.'});
    }
    else return Category.create(category).then(() => {
        return res.status(201).send({message: 'Category created.'});
    }).catch(err => {
        console.log(err);
        return res.status(500).send({message: 'Could not create category.'});
    })
    
});

categoriesRouter.put('/:id', (req, res) =>{
    let id = req.params.id;
    let category = req.body;
    if(isNaN(id))
        return res.status(400).send({message: 'Incorrect request.'});
    if(category.name == null || category.name.trim() === '')
        return res.status(400).send({message: 'Incorrect request.'});
    return Category.update(category, {where: {id: id}}).then(() => {
        return res.status(200).send({message: 'Category updated.'});
    }).catch(err => {
        console.log(err);
        return res.status(500).send({message: 'Invalid request.'});
    })
});

categoriesRouter.delete('/:id', (req, res) => {
    let id = req.params.id;
    if(isNaN(id))
        return res.status(400).send({message: 'Incorrect request.'});
    return Category.destroy({where: {id: id}}).then(() => res.status(200).send({message: 'Category deleted.'})).catch(err => {
        console.log(err);
        return res.status(500).send({message: 'Invalid request'});
    })
});

categoriesRouter.get('*', (req, res) => res.status(404).send({ message: 'Not Found' }));

module.exports = categoriesRouter;