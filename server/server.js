const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
//const { connection, User, Group } = require('../models/models');
const fs = require('fs');
const config = JSON.parse(fs.readFileSync('./client/src/appconfig.json', 'utf8'));

const port = 8081;
const corsOpt = {
    origin: `http://${config.hostAddress}:8080`
}
const application = express();
const apiRouter = require('./api-router');


application.use(cors(corsOpt));
application.use(express.json());
application.use('/api', apiRouter);
    
application.get('/', (req, res) => {
    res.status(200).send({ message: 'Works.' });
});
    
application.get('*', (req, res) => res.status(404).send({ message: 'Not Found' }));

application.listen(port, () => console.log(`Server is listening on port ${port}...`));