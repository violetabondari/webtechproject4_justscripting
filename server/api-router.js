const apiRouter = require('express').Router();

// Model Routers
const usersRouter = require('./users-router');
const categoriesRouter = require('./categories-router');
const foodRouter = require('./food-router');
const groupsRouter = require('./groups-router');

apiRouter.use('/users', usersRouter);
apiRouter.use('/food', foodRouter);
apiRouter.use('/categories', categoriesRouter);
apiRouter.use('/groups', groupsRouter);

apiRouter.get('*', (req, res) => res.status(404).send({ message: 'Not Found' }));

module.exports = apiRouter;