# Anti Food Waste App
### Team: Just Scripting
*Group 1090*

## Members
 1. Bondari Violeta-Maria - **Project Manager**
 2. Constantin Mihaela-Cristiana - **Product Owner**
 3. Bosneag Florina-Alessia - **Developer**
 4. Bucataru Armina-Doina - **Designer**

# Getting Started
Run `npm run start-app` command to start both the server and the client app.

## Client 
Can be found on port `:8080`.

## Server (REST API)
Can be found on port `:8081`.