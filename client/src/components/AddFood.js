import React, { Component } from 'react';
import config from '../appconfig.json';
import Login from './Login';

export default class AddFood extends Component{
    // constructor(props){
    //     super(props);
    //     this.state={
    //       addedDate: ' ',
    //       expirationDate: ' ',
    //       quantity: ' ',
    //       category: ' ',
    //       isAvailable : ' ',
    //       foodId: ' ',
    //       userId: ' '
    //     }
    // this.validate=this.validate.bind(this);
    

    
    // this.handleChange= (ev)=>{
    //   let currentState=this.state;
    //   currentState[ev.target.name]=ev.target.value;
    //   this.setState(currentState)
    // };
    
    // }
    // validate(){
    //     let data={
    //     addedDate:this.props.addedDate,
    //       expirationDate:this.props.expirationDate,
    //       quantity:this.props.quantity,
    //       category:this.props.category,
    //       isAvailable :this.props.isAvailable,
    //       foodId:this.props.foodId,
    //       userId: this.props.userId
    //     };
    
    
    //     fetch(`http://${config.hostAddress}:8081/api/users/id/food`, {
    //         method: 'POST', 
    //         headers: { 'Content-Type': 'application/json' },
    //         body: JSON.stringify(data)
    //     }).then(async res => {
    //         let popup = document.createElement('div');
    //         popup.innerHTML = (await res.json()).message;
            
    //         if(res.status !== 201)
    //             popup.className = 'alert alert-danger';
    //         else
    //             popup.className = 'alert alert-success';
            
    //         document.body.appendChild(popup);
    //     });
        
    //     return false;
    // }


       render() {
      return (
        <div className="card mx-auto my-3" style={{ width: "400px" }}>
            <div className="card-header">Share your food</div>
            
            <div className="card-body">
                <form className="form" submit="() => return false;">
                    <div className="form-group">
                        <label htmlFor="food-name">Name</label>
                        <input className="form-control" name="food-name" type="text" placeholder="Carbonara" required onChange={this.handleChange} value= {this.props.name}/>
                    </div>
                     <div className="form-group">
                        <label htmlFor="category-name">Category</label>
                        <input className="form-control" name="category-name" type="text" placeholder="Pasta" required required onChange={this.handleChange} value={this.props.category}/>
                    </div>
                    
                    <div className="form-group">
                        <label htmlFor="quantity">Quantity</label>
                        <input className="form-control" name="quantity" type="text" placeholder="300g" required required onChange={this.handleChange} value={this.props.quantity}/>
                    </div>
                    
                    <div className="form-group">
                        <label htmlFor="exp-date">Expire Date</label>
                        <input className="form-control" name="email" type="date" placeholder="20/01/2020" required required onChange={this.handleChange} value={this.props.expirationDate}/>
                    </div>
                    <div className="form-group">
                        <label htmlFor="add-date">Added Date</label>
                        <input className="form-control" name="add-date" type="date" required required onChange={this.handleChange} value={this.props.addedDate}/>
                        </div>
                         <div className="form-group">
                        <label htmlFor="isAvailable">Availability</label>
                        <input className="form-control" name="isAvailable" type="integer" required required onChange={this.handleChange} value={this.props.isAvailable}/>
                        </div>
                   
                    
                    <button type="submit" class="btn btn-primary" onClick={this.validate}>Add to the list!</button>

                </form>
            </div>
        </div>
      );
  }  
};