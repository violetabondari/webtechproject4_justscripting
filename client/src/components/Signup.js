import React, { Component } from 'react';

import config from '../appconfig.json';

export default class SignUp extends Component {
    constructor(props){
        super(props);
        this.state={
            firstName:' ', 
            lastName:' ',
            phoneNumber:' ',
            email:' ',
            password:' ',
        };
        
        this.validate = this.validate.bind(this);
        this.handleChange = (ev) => {
            let currentState = this.state;
            currentState[ev.target.name] = ev.target.value;
            this.setState(currentState);
        };
    }
    
    validate() {
        let data = {
            firstName: this.state.firstName,
            lastName: this.state.lastName,
            phoneNumber: this.state.phoneNumber,
            email: this.state.email,
            password: this.state.passwd
        };
        
        fetch(`http://${config.hostAddress}:8081/api/users`, {
            method: 'POST', 
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(data)
        }).then(async res => {
            let popup = document.createElement('div');
            popup.innerHTML = (await res.json()).message;
            
            if(res.status !== 201)
                popup.className = 'alert alert-danger';
            else
               { popup.className = 'alert alert-success';
               document.location.href="/";
               }
            document.body.appendChild(popup);
        });
        
        return false;
    }
    
  
    // constructor(props){
    //     super(props)
    //     this.state={firstName:this.props.item.firstName, 
    //                 lastName:this.props.item.lastName,
    //                  phoneNumber:this.props.item.phoneNumber,
    //                 email:this.props.item.email,
    //                 password:this.props.item.passwd}
    //     this.handleChange= function (evt)  {
    //         this.setState({[evt.target.name]:evt.target.values});
    //     }
    //     this.save=()=>{
    //         this.props.onSave(this.props.item.id,{firstName: this.props.item.firstName,
    //         lastName: this.props.item.lastChild,email:
    //         this.props.item.email,
    //         phoneNumber: this.props.item.phoneNumber,
    //         password:this.props.item.passwd})
    //     }
        
    //     //this.setState({isEditing});
    // }
    
  render() {
      return (
        //  if(this.state.isEditing){
        <div className="card mx-auto my-3" style={{ width: "400px" }}>
            <div className="card-header">Create an account</div>
            
            <div className="card-body">
                <form className="formSignUp" submit="return false;">
                    <div className="form-group">
                        <label htmlFor="firstName">First Name</label>
                        <input className="form-control" name="firstName" type="text" placeholder="John" required onChange={this.handleChange} value={this.props.firstName} />
                    </div>
                    
                    <div className="form-group">
                        <label htmlFor="lastName">Last Name</label>
                        <input className="form-control" name="lastName" type="text" placeholder="Doe" required onChange={this.handleChange} value={this.props.lastName}/>
                    </div>
                    <div className="form-group">
                        <label htmlFor="phoneNumber">Phone Number</label>
                        <input className="form-control" name="phoneNumber" type="phoneNumber" required onChange={this.handleChange} value={this.props.phoneNumber}/>
                        </div>
                    
                    <div className="form-group">
                        <label htmlFor="email">Email</label>
                        <input className="form-control" name="email" type="email" placeholder="john.doe@gmail.com" required onChange={this.handleChange} value={this.props.email}/>
                    </div>
                    
                    
                    <div className="form-group">
                        <label htmlFor="passwd">Password</label>
                        <input className="form-control" name="passwd" type="password" required onChange={this.handleChange} value={this.props.passwd} />
                    </div>
                    
                    <div className="form-group">
                        <button type="button" className="btn btn-primary" style={{ width: '100%' }} onClick={this.validate}>Sign Up</button>
                    </div>
                </form>
            </div>
        </div>
              
        //   }
        //   else{
        //       return<div>
        //       <h3>
        //       {
        //           this.props.item.firstName, this.props.item.lastName,this.props.item.email
        //       }</h3>
        //       <input type="button" value="delete" onClick={() => this.props.onDelete(this.props.item.id)} />
        //       </div>
        //   }
   );
  }  
};