import React, { Component } from 'react';
import config from '../appconfig.json';

export default class Home extends Component {
    constructor(props){
        super(props);
    
        this.url = `http://${config.hostAddress}`;
    
        this.state = {
            food: {
                values: [],
                isValid: undefined
            }
            
        };  
        
        this.getFood = this.getFood.bind(this);
    }
    
    getFood() {
        fetch(`${this.url}:8081/api/food?cat&usr`).then(response => response.json()).catch(err => this.setState({ food: { isValid: false }}))
        .then(data => 
        {
            if(typeof data.message === 'string')
                return this.setState({ food: { isValid: false } });
            else
                return this.setState({ food: { isValid: true, values: data }});
        }).catch(err => this.setState({ food: { isValid: false } }));
    }
    
    render() {
        this.getFood();
        
        return (
            <div>
                <div className="card my-2">
                    <div className="card-header">Welcome to the Anti-Food Waste Platform</div>
                    <div className="card-body">
                        <div className="card-text">
                            This is a social-media-like platform which aims to reduce the amount of food waste that we - as people all around the world - create
                        </div>
                    </div>
                </div>
                
                <div className="card my-2">
                    <div className="card-header">Most popular food</div>
                    
                    <div className="card-body">
                        <table className="table table-striped">
                            <thead className="thead-dark">
                                <tr>
                                    <th>Food</th>
                                    <th>Category</th>
                                    <th>No. Users</th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.state.food.values
                                    .sort((f1, f2) => f1.foodOwners.length < f2.foodOwners.length ? 1 : f1.foodOwners.length > f2.foodOwners.length ? -1 : 0)
                                    .map(f => {
                                    return (
                                        <tr>
                                            <td>{f.name}</td>
                                            <td>{f.category.name}</td>
                                            <td>{f.foodOwners.length}</td>
                                        </tr>
                                    );
                                })}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        );
    }
}