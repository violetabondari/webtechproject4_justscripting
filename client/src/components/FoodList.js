import React, { Component } from 'react';

export default class FoodList extends Component {
    constructor(props){
        super(props);
    
        this.url = 'http://3.133.109.221';
    
        this.state = {
            food: {
                values: [],
                isValid: undefined
            }
        };  
        this.getUserFood = this.getUserFood.bind(this);
    }
    
    getUserFood() {
        fetch(`${this.url}:8081/api/:id/food`).then(response => response.json()).catch(err => this.setState({ userFood: { isValid: false }}))
        .then(data => 
        {
            if(typeof data.message === 'string')
                return this.setState({ userFood: { isValid: false } });
            else
                return this.setState({ userFood: { isValid: true, values: data }});
        }).catch(err => this.setState({ userFood: { isValid: false } }));
    }
    
    render() {
        this.getUserFood();
        
        return (
            <div>
                <div className="card my-3">
                    <div className="card-header">List of the food introduced by each user</div>
                </div>
                
                <div className="card my-3">
                    
                    <div className="card-body">
                        <table className="table table-striped">
                            <thead className="thead-dark">
                                <tr>
                                    <th>Food</th>
                                    <th>Category</th>
                                    <th>Quantity</th>
                                    <th>Adding date</th>
                                    <th>Expiry date</th>
                                    <th>User Email</th>
                                </tr>
                            </thead>
                            
                        </table>
                        
                        
                    </div>
                </div>
            </div>
        );
    }
}