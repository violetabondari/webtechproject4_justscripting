import React, { Component } from 'react';
import { Persist } from 'react-persist';
import config from '../appconfig.json';


export default class Login extends Component {
    constructor(props){
        super(props);
        this.state = {
            email: '',
            password: ''
        }
        
        this.props = { handleAuth: props.handleAuth };
        
        this.handleChange = (ev) => {
            let state = this.state;
            state[ev.target.name] = ev.target.value;
            console.log("ajbfjshbvhjbesjkf");
            console.log("JEGUFSJHBFHAJ"+this.state);
            this.setState(state);
            console.log(this.state);
        }
        this.login = this.login.bind(this);
    }
    
    login(){
        fetch(`http://${config.hostAddress}:8081/api/users/login`, {
            method: 'POST',
            headers: { 
                'Content-Type': 'application/json' 
            },
            body: JSON.stringify({ email: this.state.email, password: this.state.password})
        }).then(async res =>{
            if(res.status !== 200){
                let message = document.createElement('div');
                message.className = 'alert alert-danger';
                message.innerText = (await res.json()).message;
                document.body.appendChild(message);
            }
            else{
               
                let message = document.createElement('div');
                message.className = 'alert alert-success';
                message.innerText = (await res.json()).message;
                document.body.appendChild(message);
                 console.log("before handleAuth");
                 
                 
               this.props.handleAuth(true) ;
               console.log("after handleAuth");
                document.location.href="/"
            }
           
        });
    }
    
    render() {
        return (
            
            <div className="card mx-auto my-5" style={{ width: "400px" }}>
                <div className="card-header">Login to your account</div>
                
                <div className="card-body">
                    <form className="form" submit="() => return false;">
                        <div className="form-group">
                            <label htmlFor="email">Email</label>
                            <input className="form-control" 
                                name="email" 
                                type="email" 
                                placeholder="john.doe@gmail.com" 
                                value={this.state.email} 
                                onChange={this.handleChange}
                                required />
                        </div>
                        
                        <div className="form-group">
                            <label htmlFor="password">Password</label>
                            <input className="form-control" 
                                name="password" 
                                type="password" 
                                value={this.state.password} 
                                onChange={this.handleChange}
                                required />
                        </div>
                        
                        <button type="button" className="btn btn-primary" onClick={this.login}>Login</button>
                        
                     
                    </form>
                </div>
            </div>
        );
    }  
};