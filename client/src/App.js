import React, { Component } from 'react';
import './App.css';
//import { render } from 'react-dom';
import { Route } from 'react-router-dom';
import Home from './components/Home';
import Login from './components/Login';
import SignUp from './components/Signup';
import AddFood from './components/AddFood';
import FoodList from './components/FoodList';

class App extends Component {

 constructor(){
   super();
   this.state = { isAuthenticated: false};
   this.handleAuthentication = (isLoggedIn) => {
     this.setState({ isAuthenticated: isLoggedIn});
        console.log(this.state);
   }
   
  
  this.routes = () => {
     if(this.state.isAuthenticated) // user is logged in 
       return ( <div className="container">
                  <Route exact path="/" component={Home} />
                  <Route path="/addfood" component={AddFood}/>
                  <Route path="/list" component={FoodList}/>
                </div>);
     else
        return ( <div className="container">
                  <Route exact path="/" component={Home} />
                  <Route path="/login" render = { (props) => <Login {...props} handleAuth={this.handleAuthentication} />}/>
                  <Route path="/signup" component={SignUp} />
                </div>);
   };
   this.navbar = () => {
     if(this.state.isAuthenticated)
      return (<div className="navbar-nav">
                <a className="nav-item nav-link active" href="/">Home</a>
                <a className="nav-item nav-link" href="/addfood">Add Food</a>
                <a className="nav-item nav-link" href="/list">Food List</a>
              </div>);
      else
        return (<div className="navbar-nav">
                  <a className="nav-item nav-link active" href="/">Home</a>
                  <a className="nav-item nav-link" href="/login">Login</a>
                  <a className="nav-item nav-link" href="/signup">Sign up</a>
                </div>);
   };
 } 
 

 render(){

  return (
    <div>
      <nav className="navbar navbar-expand-md navbar-dark bg-dark">
        <a className="navbar-brand" href="/">Anti-Food Waste Platform</a>
        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-buttons" aria-controls="navbar-buttons" aria-expanded="false">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbar-buttons">
          {this.navbar()}
        </div>
      </nav>
      
      {this.routes()}
    </div>
  );
}
}
export default App;
